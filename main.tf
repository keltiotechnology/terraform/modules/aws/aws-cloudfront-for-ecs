resource "aws_cloudfront_distribution" "default" {
  origin {
    domain_name = var.aws_lb_dns_name
    origin_id   = var.aws_lb_id
    custom_origin_config {
      http_port              = 80
      https_port             = 443
      origin_protocol_policy = "match-viewer"
      origin_ssl_protocols   = ["TLSv1"]
    }
  }

  enabled         = var.enable_cloudfront
  is_ipv6_enabled = var.enable_ipv6
  comment         = var.comment
  aliases         = var.alternative_domain_names
  price_class     = var.price_class

  default_cache_behavior {
    allowed_methods  = var.cloudfront_allowed_methods
    cached_methods   = var.cloudfront_cache_methods
    target_origin_id = var.aws_lb_id

    viewer_protocol_policy = var.cloudfront_viewer_protocol_policy
    min_ttl                = var.cloudfront_min_ttl
    default_ttl            = var.cloudfront_default_ttl
    max_ttl                = var.cloudfront_max_ttl
    compress               = var.cloudfront_compress

    forwarded_values {
      query_string = false
      headers      = ["*"]

      cookies {
        forward = "none"
      }
    }
  }

  restrictions {
    geo_restriction {
      restriction_type = var.geo_restriction_restriction_type
      locations        = var.geo_restriction_locations
    }
  }

  tags = var.tags

  viewer_certificate {
    cloudfront_default_certificate = var.cloudfront_default_certificate
    acm_certificate_arn            = var.acm_certificate_arn
    ssl_support_method             = "sni-only"
    minimum_protocol_version       = "TLSv1"
  }
}
