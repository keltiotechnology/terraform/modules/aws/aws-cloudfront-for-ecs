variable "aws_lb_dns_name" {
  type        = string
  description = "DNS name of AWS Load balancer"
}

variable "aws_lb_id" {
  type        = string
  description = "Name of the AWS Load balancer e.g. my-load-balancer"
}

variable "enable_cloudfront" {
  type        = bool
  description = ""
  default     = true
}

variable "enable_ipv6" {
  type        = bool
  description = ""
  default     = false
}

variable "comment" {
  type        = string
  description = ""
  default     = ""
}

variable "alternative_domain_names" {
  type        = list(string)
  description = ""
}

variable "cloudfront_allowed_methods" {
  type        = list(string)
  description = "Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin."
  default     = ["DELETE", "GET", "HEAD", "OPTIONS", "PATCH", "POST", "PUT"]
}

variable "cloudfront_cache_methods" {
  type        = list(string)
  description = "Controls whether CloudFront caches the response to requests using the specified HTTP methods."
  default     = ["GET", "HEAD"]
}

variable "cloudfront_viewer_protocol_policy" {
  type    = string
  default = "redirect-to-https"
}

variable "cloudfront_min_ttl" {
  type        = number
  description = "The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated. Defaults to 0 seconds"
  default     = 0
}

variable "cloudfront_default_ttl" {
  type        = number
  description = "The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header."
  default     = 3600
}

variable "cloudfront_max_ttl" {
  type        = number
  description = "The maximum amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated. Only effective in the presence of Cache-Control max-age, Cache-Control s-maxage, and Expires headers."
  default     = 86400
}

variable "cloudfront_compress" {
  type        = bool
  description = "Whether you want CloudFront to automatically compress content for web requests that include Accept-Encoding: gzip in the request header (default: false)."
  default     = false
}

variable "price_class" {
  type    = string
  default = "PriceClass_200"
}

variable "geo_restriction_restriction_type" {
  type        = string
  description = "The method that you want to use to restrict distribution of your content by country: none, whitelist, or blacklist"
  default     = "none"
}

variable "geo_restriction_locations" {
  type        = list(string)
  description = "The ISO 3166-1-alpha-2 codes for which you want CloudFront either to distribute your content (whitelist) or not distribute your content (blacklist)."
  default     = []
}

variable "tags" {
  type        = map(string)
  description = " A map of tags to assign to the resource. If configured with a provider default_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level."
  default = {
    "Name" = "Production"
  }
}


variable "cloudfront_default_certificate" {
  type        = bool
  description = "true if you want viewers to use HTTPS to request your objects and you're using the CloudFront domain name for your distribution. Specify this, acm_certificate_arn, or iam_certificate_id."
  default     = true
}

variable "acm_certificate_arn" {
  type        = string
  description = "The ARN of the AWS Certificate Manager certificate that you wish to use with this distribution. Specify this, cloudfront_default_certificate, or iam_certificate_id. The ACM certificate must be in US-EAST-1."
}
