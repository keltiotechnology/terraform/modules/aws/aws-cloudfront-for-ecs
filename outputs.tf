output "cloudfrond_domain_name" {
  description = "Domain name of cloudfront"
  value       = aws_cloudfront_distribution.default.domain_name
}

output "cloudfront_hosted_zone" {
  description = "Hosted zone ID of cloudfront"
  value       = aws_cloudfront_distribution.default.hosted_zone_id
}

output "cloudfront_id" {
  description = "ID of cloudfront distribution"
  value       = aws_cloudfront_distribution.default.id
}