# Cloudfront distribution for  Application Load Balancer and ECS

## Usage

```hcl-terraform
module "cloudfront" {
  source                      = "https://gitlab.com/keltiotechnology/terraform/modules/aws/aws-cloudfront-for-ecs"
  acm_certificate_arn         = module.certificate_manager.aws_acm_certificate_arn
  aws_lb_dns_name             = module.vpc.aws_lb_dns_name
  aws_lb_id                   = module.vpc.aws_lb_name
  alternative_domain_names    = [var.domain_name]
}

resource "aws_route53_record" "alias" {
  zone_id                     = var.hosted_zone_id
  name                        = var.domain_name
  type                        = "A"

  alias {
    name                      = module.cloudfront.cloudfrond_domain_name
    zone_id                   = module.cloudfront.cloudfront_hosted_zone
    evaluate_target_health    = false
  }
}
```

<!-- BEGINNING OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
## Requirements

No requirements.

## Providers

| Name | Version |
|------|---------|
| <a name="provider_aws"></a> [aws](#provider\_aws) | 3.56.0 |

## Modules

No modules.

## Resources

| Name | Type |
|------|------|
| [aws_cloudfront_distribution.default](https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/cloudfront_distribution) | resource |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| <a name="input_acm_certificate_arn"></a> [acm\_certificate\_arn](#input\_acm\_certificate\_arn) | The ARN of the AWS Certificate Manager certificate that you wish to use with this distribution. Specify this, cloudfront\_default\_certificate, or iam\_certificate\_id. The ACM certificate must be in US-EAST-1. | `string` | n/a | yes |
| <a name="input_alternative_domain_names"></a> [alternative\_domain\_names](#input\_alternative\_domain\_names) | n/a | `list(string)` | n/a | yes |
| <a name="input_aws_lb_dns_name"></a> [aws\_lb\_dns\_name](#input\_aws\_lb\_dns\_name) | DNS name of AWS Load balancer | `string` | n/a | yes |
| <a name="input_aws_lb_id"></a> [aws\_lb\_id](#input\_aws\_lb\_id) | Name of the AWS Load balancer e.g. my-load-balancer | `string` | n/a | yes |
| <a name="input_cloudfront_allowed_methods"></a> [cloudfront\_allowed\_methods](#input\_cloudfront\_allowed\_methods) | Controls which HTTP methods CloudFront processes and forwards to your Amazon S3 bucket or your custom origin. | `list(string)` | <pre>[<br>  "DELETE",<br>  "GET",<br>  "HEAD",<br>  "OPTIONS",<br>  "PATCH",<br>  "POST",<br>  "PUT"<br>]</pre> | no |
| <a name="input_cloudfront_cache_methods"></a> [cloudfront\_cache\_methods](#input\_cloudfront\_cache\_methods) | Controls whether CloudFront caches the response to requests using the specified HTTP methods. | `list(string)` | <pre>[<br>  "GET",<br>  "HEAD"<br>]</pre> | no |
| <a name="input_cloudfront_compress"></a> [cloudfront\_compress](#input\_cloudfront\_compress) | Whether you want CloudFront to automatically compress content for web requests that include Accept-Encoding: gzip in the request header (default: false). | `bool` | `false` | no |
| <a name="input_cloudfront_default_certificate"></a> [cloudfront\_default\_certificate](#input\_cloudfront\_default\_certificate) | true if you want viewers to use HTTPS to request your objects and you're using the CloudFront domain name for your distribution. Specify this, acm\_certificate\_arn, or iam\_certificate\_id. | `bool` | `true` | no |
| <a name="input_cloudfront_default_ttl"></a> [cloudfront\_default\_ttl](#input\_cloudfront\_default\_ttl) | The default amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request in the absence of an Cache-Control max-age or Expires header. | `number` | `3600` | no |
| <a name="input_cloudfront_max_ttl"></a> [cloudfront\_max\_ttl](#input\_cloudfront\_max\_ttl) | The maximum amount of time (in seconds) that an object is in a CloudFront cache before CloudFront forwards another request to your origin to determine whether the object has been updated. Only effective in the presence of Cache-Control max-age, Cache-Control s-maxage, and Expires headers. | `number` | `86400` | no |
| <a name="input_cloudfront_min_ttl"></a> [cloudfront\_min\_ttl](#input\_cloudfront\_min\_ttl) | The minimum amount of time that you want objects to stay in CloudFront caches before CloudFront queries your origin to see whether the object has been updated. Defaults to 0 seconds | `number` | `0` | no |
| <a name="input_cloudfront_viewer_protocol_policy"></a> [cloudfront\_viewer\_protocol\_policy](#input\_cloudfront\_viewer\_protocol\_policy) | n/a | `string` | `"redirect-to-https"` | no |
| <a name="input_comment"></a> [comment](#input\_comment) | n/a | `string` | `""` | no |
| <a name="input_enable_cloudfront"></a> [enable\_cloudfront](#input\_enable\_cloudfront) | n/a | `bool` | `true` | no |
| <a name="input_enable_ipv6"></a> [enable\_ipv6](#input\_enable\_ipv6) | n/a | `bool` | `false` | no |
| <a name="input_geo_restriction_locations"></a> [geo\_restriction\_locations](#input\_geo\_restriction\_locations) | The ISO 3166-1-alpha-2 codes for which you want CloudFront either to distribute your content (whitelist) or not distribute your content (blacklist). | `list(string)` | `[]` | no |
| <a name="input_geo_restriction_restriction_type"></a> [geo\_restriction\_restriction\_type](#input\_geo\_restriction\_restriction\_type) | The method that you want to use to restrict distribution of your content by country: none, whitelist, or blacklist | `string` | `"none"` | no |
| <a name="input_price_class"></a> [price\_class](#input\_price\_class) | n/a | `string` | `"PriceClass_200"` | no |
| <a name="input_tags"></a> [tags](#input\_tags) | A map of tags to assign to the resource. If configured with a provider default\_tags configuration block present, tags with matching keys will overwrite those defined at the provider-level. | `map(string)` | <pre>{<br>  "Name": "Production"<br>}</pre> | no |

## Outputs

| Name | Description |
|------|-------------|
| <a name="output_cloudfrond_domain_name"></a> [cloudfrond\_domain\_name](#output\_cloudfrond\_domain\_name) | Domain name of cloudfront |
| <a name="output_cloudfront_hosted_zone"></a> [cloudfront\_hosted\_zone](#output\_cloudfront\_hosted\_zone) | Hosted zone ID of cloudfront |
| <a name="output_cloudfront_id"></a> [cloudfront\_id](#output\_cloudfront\_id) | ID of cloudfront distribution |
<!-- END OF PRE-COMMIT-TERRAFORM DOCS HOOK -->
